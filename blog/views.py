from datetime import datetime

from django.contrib.auth.models import User
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse

from blog.forms import PostForm
from blog.models import Post


# model manager
# objects

def blog_list(request):
    from django.forms import formset_factory
    # ArticleFormSet = formset_factory(ArticleForm, extra=2)

    # http request
    post = Post.objects.all()
    num_vis = request.session.get('visit_count', 1)
    request.session['visit_count'] = num_vis + 1
    front_end_ma_patahune_data = {'post_table_ko_data': post, 'test_value': 1, 'num_visits': num_vis,
                                  # 'formset': ArticleFormSet(initial=[
                                  #     {
                                  #         'title': 'Django is now open source',
                                  #         'pub_date': datetime.now(),
                                  #     },
                                  # ])
                                  }
    return render(request, 'list.html', context=front_end_ma_patahune_data)


def blog_detail(request, id):
    post = Post.objects.get(pk=id)
    return render(request, 'details.html', context={'post': post})


def blog_create(request):
    # form = PostForm(instance=Post.objects.get(pk=17))
    # import pdb
    # pdb.set_trace()
    if request.method == 'POST':
        # f = PostForm(request.POST, request.FILES)
        # if f.is_valid():
        #     f.save()
        author = request.POST.get('author', request.user)
        if author:
            author_object = User.objects.get(username=author)
        title = request.POST.get('title', 'default title')
        body = request.POST.get('body', 'Content ContentContentContentContentContentContentContentContent')
        Post.objects.create(author=author_object, title=title, body=body, photo=request.FILES.get('photo'))
        # return render(request, 'list.html')
        return redirect(reverse('blog:list_post'))
    context = {'users': User.objects.all()}
    return render(request, 'create.html', context=context)


"""
static:
    js,
    css,
    image
    
    Static Root:
        --> The directory where we collect all of the static files in the system
    static Url:
        --> Url to set inorder to access the static files
    
    Static File directory:
        -->  Where we locally store the static files
        
        
    Media:
        --> 
"""


def create_blog_using_django_form(request):
    f = PostForm()
    if request.method == 'POST':
        f = PostForm(request.POST, request.FILES)
        valid_check = f.is_valid()
        if valid_check:
            f.save()
            return redirect(reverse('blog:list_post'))

    return render(request, 'create_post_django_form.html', context={'form': f})
    pass
