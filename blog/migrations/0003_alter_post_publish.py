# Generated by Django 3.2 on 2021-04-21 12:23

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_change_time_zone'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='publish',
            field=models.DateTimeField(default=datetime.datetime(2021, 4, 21, 12, 23, 23, 975867, tzinfo=utc)),
        ),
    ]
