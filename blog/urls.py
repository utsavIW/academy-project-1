from django.urls import path

from blog.views import *

app_name = 'blog'

urlpatterns = [
    path('list/', blog_list, name='list_post'),
    path('detail/<int:id>/',blog_detail, name='blog_detail'),
    path('create/',blog_create, name='blog_create'),
    path('create_using_form/',create_blog_using_django_form, name='blog_create_using_form'),
]
