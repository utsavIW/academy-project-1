from django.contrib.auth.models import User
from django.db import models

from django.utils import timezone

# create your models here
class Post(models.Model):
    title = models.CharField(max_length=250)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    body = models.TextField()
    publish = models.DateTimeField(default=timezone.now)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    photo = models.ImageField(upload_to='blogs/%Y/%m/%d', blank=True, null=True) # blogs/file_name

    # def __str__(self):
    #     return self.title